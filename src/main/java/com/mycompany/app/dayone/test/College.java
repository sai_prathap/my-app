package com.mycompany.app.dayone.test;

import java.util.List;

public class College {
		
		private String collegeName;
		private String address;
		private List<StudentClass> classes;
		
		
		public boolean equals(Object obj) {
			if(this == obj) return true;
			if(obj == null || getClass() !=obj.getClass()) return false;
			College clg = (College) obj;
			return getCollegeName().equals(clg.getCollegeName()) && getAddress().equals(clg.getAddress()) && getClasses().equals(clg.getClasses());
		}
		
		
		public String getCollegeName() {
			return collegeName;
		}
		public void setCollegeName(String collegeName) {
			this.collegeName = collegeName;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public List<StudentClass> getClasses() {
			return classes;
		}
		public void setClasses(List<StudentClass> classes) {
			this.classes = classes;
		}
		
		

	}


