package com.mycompany.app.dayone.test;

public class DuckExample {
	
	public static void main(String[] args) {
		DuckMa dm = new DuckMa();
		
		dm.setDuck(new WhiteDuck());
		dm.fly();
		dm.quack();
		
		dm.setDuck(new Brown());
		dm.fly();
		dm.quack();
		
		dm.setDuck(new Malavad());
		dm.fly();
		dm.quack();
		
		dm.setDuck(new Decoy());
		dm.fly();
		dm.quack();
		
		
		
		
	}

}
