package com.mycompany.app.dayone.test;


import java.util.ArrayList;
	import java.util.HashSet;
	import java.util.List;
	import java.util.Set;

	public class Result {

		public static void main(String[] args) {
			
			Student s1 = new Student();
			s1.setName("Prathap");
			s1.setRollNo("079866");
			
			Student s2 = new Student();
			s2.setName("Prathap");
			s2.setRollNo("iu786y");
			
			StudentClass sc = new StudentClass();
			
			Set<Student> st = new HashSet<Student>();
			st.add(s1);
			
			sc.setcName("tenth");
			sc.setStudents(st);
			
			College cc = new College();
			 
			List<StudentClass> ss = new ArrayList<StudentClass>();
			ss.add(sc);
			
			cc.setCollegeName("abc");
			cc.setAddress("Guntur");
			cc.setClasses(ss);
			
			System.out.println(cc.equals(cc));
			
			
			sc.setTotalNoOfStudents(10);
			
			System.out.println(sc.equals(sc));
			
			System.out.println(s1.equals(s2));

		}

	}


