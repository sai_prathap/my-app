package com.mycompany.app.dayone.test;

public class Student {
	
		private String name;
		private String rollNo;
		
		@Override
		public boolean equals(Object obj) {
			
			if(this == obj) return true;
			
			if(obj == null || getClass() != obj.getClass()) return false;
			
			
			
			return this.getName().equals( ((Student)obj).getName()) && this.getRollNo().equals(((Student)obj).getRollNo());
		}
		
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getRollNo() {
			return rollNo;
		}
		public void setRollNo(String rollNo) {
			this.rollNo = rollNo;
		}
		
		
	}


