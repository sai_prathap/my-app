package com.mycompany.app.dayone.test;

public interface Duck {
	
	public void fly();
	public void quack();
}
 
class DuckMa{
	private Duck duck;

	public void setDuck(Duck duck) {
		this.duck = duck;
	}
	
	public void fly() {
		duck.fly();
	}
	public void quack() {
		duck.quack();
	}
}

class WhiteDuck implements Duck {

	@Override
	public void fly() {
		System.out.println("I Will FLy !");
		
	}

	@Override
	public void quack() {
		System.out.println("I Will Quack !");
		
	}
}
	class Brown implements Duck{

		@Override
		public void fly() {
			System.out.println("I Will Fly !!");
			
		}

		@Override
		public void quack() {
			System.out.println("I will Quack !!");
			 
			
		}
		
	}
		
		class Malavad implements Duck{

			@Override
			public void fly() {
				System.out.println("I Will Fly !!");
				
			}

			@Override
			public void quack() {
			System.out.println("I will Quack !!");
			}
			}
			
			class Decoy implements Duck{

				@Override
				public void fly() {
					System.out.println("I Will Fly !!!");
					
				}

				@Override
				public void quack() {
					System.out.println("I will Quack !!!");
					
				}
				
				
				
			}


