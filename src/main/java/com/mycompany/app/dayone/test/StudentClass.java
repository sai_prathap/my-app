package com.mycompany.app.dayone.test;


import java.util.Set;

	public class StudentClass {
		
		private String cName;
		private int totalNoOfStudents;
		private Set<Student> students;
		
		
		public String getcName() {
			return cName;
		}
		public void setcName(String cName) {
			this.cName = cName;
		}
		public int getTotalNoOfStudents() {
			return totalNoOfStudents;
		}
		public void setTotalNoOfStudents(int totalNoOfStudents) {
			this.totalNoOfStudents = totalNoOfStudents;
		}
		
		public void setStudents(Set<Student> students) {
			this.students = students;
		}
		public Set<Student> getStudents() {
			return students;
		}

		
		public boolean equals(Object obj) {
			if(this == obj) return true;
			if(obj == null || getClass()!=obj.getClass()) return false;
			
			StudentClass s2 = (StudentClass) obj;
			
			return this.getcName().equals(s2.getcName()) && this.getTotalNoOfStudents() == (s2.getTotalNoOfStudents()) && this.getStudents().equals(s2.getStudents());
		
		}
		
		
		
		
		
		
	}



